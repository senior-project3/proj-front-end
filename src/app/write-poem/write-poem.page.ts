import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { PoemService } from '../services/poem.service';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { AuthService } from '../services/auth.service';


@Component({
  selector: 'app-write-poem',
  templateUrl: './write-poem.page.html',
  styleUrls: ['./write-poem.page.scss'],
})
export class WritePoemPage implements OnInit, OnDestroy {

  poemForm: FormGroup
  eightPoemForm: FormGroup;
  fourPoemForm: FormGroup;
  typeSelected = ['กลอนแปดสุภาพ', 'โคลงสี่สุภาพ'];
  keySection = [
    'section1', 'section2', 'section3', 'section4',
  ]
  key = {};
  sections: number = 4;
  suggestWordEnabled: boolean = true;
  suggestWordList;
  targetSuggestWordField;
  private subscription: Subscription;

  constructor(private formBuilder: FormBuilder,
              private poemService: PoemService,
              private router: Router,
              private auth: AuthService) { }

  ngOnInit() {
    this.poemForm = this.formBuilder.group({
      name: ['', Validators.required],
      type: ['กลอนแปดสุภาพ', Validators.required],
      privacy: [false],
    });
    this.formReset();
  }

  onSubmit() {
    if(this.poemForm.invalid) {
      return ;
    }
    if (this.eightPoemForm.invalid && this.poemForm.controls.type.value == 'กลอนแปดสุภาพ') {
      return ;
    }
    if (this.fourPoemForm.invalid && this.poemForm.controls.type.value == 'โคลงสี่สุภาพ') {
      return ;
    }

    const data = {
      name: this.poemForm.controls.name.value,
      type: this.poemForm.controls.type.value,
      privacy: this.poemForm.controls.privacy.value,
      author: this.auth.getCurrentUser().id,
      content: this.getContent()
    }
    this.subscription = this.poemService.write(data)
      .subscribe(
        data => {
          this.poemForm.reset();
          this.formReset();
          let suggestWord: string = '';
          Object.keys(data['suggest_word']).map(word=> {
            if(word){
              suggestWord += word + '=' + data['suggest_word'][word] + '&';
            }
          })
          console.log(suggestWord)
          this.router.navigateByUrl('tabs/poem/'+data['id']+`?${suggestWord}`);
      });
  }

  getContent() {
    const content = [];
    if(!this.key) {
      return;
    }
    const keyType = (this.poemForm.controls.type.value == "กลอนแปดสุภาพ")? 'eightPoem' : 'fourPoem';
    for(let i=1; i<=this.sections; i++) {
      let sectionString = [];
      for(let j=1; j<=this.key[keyType]['section'+i].length; j++){
        // console.log('section'+i+'_'+j)
        if(this.poemForm.controls.type.value == "กลอนแปดสุภาพ"){
          if(this.eightPoemForm.controls['section'+i+'_'+j].value != "")
            sectionString.push(this.eightPoemForm.controls['section'+i+'_'+j].value);
        }
        else {
          if(this.fourPoemForm.controls['section'+i+'_'+j].value != "")
            sectionString.push(this.fourPoemForm.controls['section'+i+'_'+j].value);
        }
      }
      if(sectionString) {
        content.push(sectionString);
      }
    }
    if(this.poemForm.controls.type.value == "กลอนแปดสุภาพ") {
      for(let i=0; i<content.length; i+=4) {
        if(content[i].length == 0 && content[i+1].length == 0 && content[i+2].length == 0 && content[i+3].length == 0) {
          for(let j=0; j<4; j++)
            content.pop();
        }  
      } 
    } else {
      for(let i=0; i<content.length; i+=8) {
        if(content[i].length == 0 && content[i+1].length == 0 && content[i+2].length == 0 && content[i+3].length == 0
          && content[i+4].length == 0 && content[i+5].length == 0 && content[i+6].length == 0 && content[i+7].length == 0) {
          for(let j=0; j<8; j++)
            content.pop();
        }  
      } 
    }
    console.log(content);
    return content;
  }

  addChapter() {
    if(this.poemForm.controls.type.value == 'กลอนแปดสุภาพ') {
      for(let i = 1; i<=4; i++) {
        this.keySection.push('section'+(i+this.sections));
        this.key['eightPoem']['section'+(i+this.sections)] = [];
        for(let j = 1; j<=8; j++){
          this.key['eightPoem']['section'+(i+this.sections)].push('section'+(i+this.sections)+'_'+j);
          this.eightPoemForm.addControl('section'+(i+this.sections)+'_'+j, new FormControl(''));
        } 
      }
      this.sections += 4;
    } else {
      for(let i = 1; i<=8; i++) {
        this.keySection.push('section'+(i+this.sections));
        this.key['fourPoem']['section'+(i+this.sections)] = [];
        // console.log(i+this.sections)
        switch(i%8) {
          case 1:
          case 3:
          case 5:
          case 7: for(let j = 1; j<=5; j++) {
                    this.key['fourPoem']['section'+(i+this.sections)].push('section'+(i+this.sections)+'_'+j);
                    this.fourPoemForm.addControl('section'+(i+this.sections)+'_'+j, new FormControl(''));
                  }
                  break;
          case 0:
          case 2:
          case 6: for(let j = 1; j<=4; j++) {
                    this.key['fourPoem']['section'+(i+this.sections)].push('section'+(i+this.sections)+'_'+j);
                    this.fourPoemForm.addControl('section'+(i+this.sections)+'_'+j, new FormControl(''));
                  }
                  break;
          case 4: for(let j = 1; j<=2; j++) {
                    this.key['fourPoem']['section'+(i+this.sections)].push('section'+(i+this.sections)+'_'+j);
                    this.fourPoemForm.addControl('section'+(i+this.sections)+'_'+j, new FormControl(''));
                  }
                  break;
        }
      }
      this.sections += 8;
    }
  }

  removeChapter() {
    if(this.poemForm.controls.type.value == 'กลอนแปดสุภาพ') {
      if(this.sections == 4) {
        return
      }
      for(let i = 0; i<4; i++) {
        this.keySection.pop();
        delete this.key['eightPoem']['section'+(this.sections-i)];
        for(let j = 1; j<=8; j++){
          this.eightPoemForm.removeControl('section'+(this.sections-i)+'_'+j);
        } 
      }
      this.sections -= 4;
    } else {
      if(this.sections == 8) {
        return
      }
      for(let i = 0; i<8; i++) {
        this.keySection.pop();
        delete this.key['fourPoem']['section'+(this.sections-i)];
        for(let j = 1; j<=8; j++){
          switch((i%8)+1) {
            case 1:
            case 3:
            case 5:
            case 7: for(let j = 1; j<=5; j++) {
                      this.fourPoemForm.removeControl('section'+(this.sections-i)+'_'+j);
                    }
                    break;
            case 0:
            case 2:
            case 6: for(let j = 1; j<=4; j++) {
                      this.fourPoemForm.removeControl('section'+(this.sections-i)+'_'+j);
                    }
                    break;
            case 4: for(let j = 1; j<=2; j++) {
                      this.fourPoemForm.removeControl('section'+(this.sections-i)+'_'+j);
                    }
                    break;
          }
        } 
      }
      this.sections -= 8;
    }
  }

  eightFormInit() {
    this.eightPoemForm = this.formBuilder.group({
      section1_1: [''], section1_2: [''], section1_3: [''],
      section1_4: [''], section1_5: [''], section1_6: [''],
      section1_7: [''], section1_8: [''],
      section2_1: [''], section2_2: [''], section2_3: [''],
      section2_4: [''], section2_5: [''], section2_6: [''],
      section2_7: [''], section2_8: [''],
      section3_1: [''], section3_2: [''], section3_3: [''], 
      section3_4: [''], section3_5: [''], section3_6: [''],
      section3_7: [''], section3_8: [''],
      section4_1: [''], section4_2: [''], section4_3: [''],
      section4_4: [''], section4_5: [''], section4_6: [''],
      section4_7: [''], section4_8: [''],
    });
  }

  fourFormInit() {
    this.fourPoemForm = this.formBuilder.group({
      section1_1: [''], section1_2: [''], section1_3: [''],
      section1_4: [''], section1_5: [''],
      section2_1: [''], section2_2: [''], section2_3: [''],
      section2_4: [''], 
      section3_1: [''], section3_2: [''], section3_3: [''], 
      section3_4: [''], section3_5: [''], 
      section4_1: [''], section4_2: [''],
      section5_1: [''], section5_2: [''], section5_3: [''], 
      section5_4: [''], section5_5: [''],
      section6_1: [''], section6_2: [''], section6_3: [''],
      section6_4: [''], 
      section7_1: [''], section7_2: [''], section7_3: [''], 
      section7_4: [''], section7_5: [''],
      section8_1: [''], section8_2: [''], section8_3: [''],
      section8_4: [''], 
    });
  }
  
  formReset(){
    this.eightFormInit();
    this.fourFormInit();
    this.key = {
      eightPoem: {
        section1: [
          'section1_1', 'section1_2', 'section1_3', 'section1_4',
          'section1_5', 'section1_6', 'section1_7', 'section1_8'
        ],
        section2: [
          'section2_1', 'section2_2', 'section2_3', 'section2_4',
          'section2_5', 'section2_6', 'section2_7', 'section2_8'
        ],
        section3: [
          'section3_1', 'section3_2', 'section3_3', 'section3_4',
          'section3_5', 'section3_6', 'section3_7', 'section3_8'
        ],
        section4: [
          'section4_1', 'section4_2', 'section4_3', 'section4_4',
          'section4_5', 'section4_6', 'section4_7', 'section4_8'
        ],
      },
      fourPoem: {
        section1: [
          'section1_1', 'section1_2', 'section1_3', 'section1_4',
          'section1_5',
        ],
        section2: [
          'section2_1', 'section2_2', 'section2_3', 'section2_4',
        ],
        section3: [
          'section3_1', 'section3_2', 'section3_3', 'section3_4',
          'section3_5',
        ],
        section4: [
          'section4_1', 'section4_2',
        ],
        section5: [
          'section5_1', 'section5_2', 'section5_3', 'section5_4',
          'section5_5',
        ],
        section6: [
          'section6_1', 'section6_2', 'section6_3', 'section6_4',
        ],
        section7: [
          'section7_1', 'section7_2', 'section7_3', 'section7_4',
          'section7_5',
        ],
        section8: [
          'section8_1', 'section8_2', 'section8_3', 'section8_4',
        ],
      },
    };
  }

  onTypeChange() {
    if (this.poemForm.controls.type.value == "กลอนแปดสุภาพ") { 
      this.keySection = ['section1', 'section2', 'section3', 'section4'];
      this.formReset();
      this.sections = 4;
    } else {
      this.keySection = ['section1', 'section2', 'section3', 'section4','section5', 'section6', 'section7', 'section8'];
      this.formReset();
      this.sections = 8;
    }
  }


  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

  suggestWord(sec){
    console.log(sec, this.poemForm.controls.type.value)
    if(sec.slice(-1) < 3){
      console.log('too few input ')
      return ;
    }
    if(!this.suggestWordEnabled) {
      console.log('wait a bit')
      return ;
    }
    let word1, word2;
    if(this.poemForm.controls.type.value == 'กลอนแปดสุภาพ') {
      word1 = this.eightPoemForm.controls[sec.substr(0, sec.length - 1) + (parseInt(sec.slice(-1))-2)].value;
      word2 = this.eightPoemForm.controls[sec.substr(0, sec.length - 1) + (parseInt(sec.slice(-1))-1)].value;  
    } else {
      word1 = this.fourPoemForm.controls[sec.substr(0, sec.length - 1) + (parseInt(sec.slice(-1))-2)].value;
      word2 = this.fourPoemForm.controls[sec.substr(0, sec.length - 1) + (parseInt(sec.slice(-1))-1)].value;  
    }
    setTimeout(() => {
      // if((this.poemForm.controls.type.value == 'กลอนแปดสุภาพ' && this.eightPoemForm.controls[sec].value == '')||
      //    (this.poemForm.controls.type.value == 'โคลงสี่สุภาพ' && this.fourPoemForm.controls[sec].value == '')) {
      //     if(word1.length > 0 && word2.length > 0){
      //       this.poemService.suggestWord([word1, word2]).subscribe(
      //         result => {
      //           this.suggestWordList = result;
      //           this.targetSuggestWordField = sec;
      //       }); 
      //     }
      //     this.suggestWordEnabled = false;    
      // } 
    },2000);
  }

  completeSuggestWordField(){
    this.targetSuggestWordField = "";
    this.suggestWordList = [];
    this.suggestWordEnabled = true;
  }
}
