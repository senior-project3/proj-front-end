import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { WritePoemPage } from './write-poem.page';

const routes: Routes = [
  {
    path: '',
    component: WritePoemPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class WritePoemPageRoutingModule {}
