import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { AuthService } from '../services/auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-register',
  templateUrl: './register.page.html',
  styleUrls: ['./register.page.scss'],
})
export class RegisterPage implements OnInit {

  registerForm: FormGroup;
  loading: boolean = false;
  error: boolean = false;
  duplicate: boolean = false;

  constructor(private formBuilder: FormBuilder, private auth: AuthService, private router: Router) { }

  ngOnInit() {
    this.registerForm = this.formBuilder.group({
      email: ['', Validators.required],
      username: ['', Validators.required],
      password: ['', Validators.required],
      passwordConfirm: ['', Validators.required],
    });
  }

  register() {
    this.loading = true;
    if(this.registerForm.controls.password.value != this.registerForm.controls.passwordConfirm.value) {
      this.error = true;
      return
    }
    if(this.registerForm.invalid){
      this.error = true;
      return
    }
    const user = {
      email: this.registerForm.controls.email.value,
      username: this.registerForm.controls.username.value,
      password: this.registerForm.controls.password.value
    };
    this.auth.register(user).subscribe(res => {
      if(res['message'] === "Register Success") {
        this.registerForm.reset();
        this.router.navigateByUrl('/login')  
      } else if(res['message'] == "Duplicate User") {
        this.duplicate = true;
      }
    });
    this.loading = false;
  }

  changed() {
    this.error = false;
    this.loading = false;
  }
}
