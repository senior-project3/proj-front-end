import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { AppendPoemPage } from './append-poem.page';

describe('AppendPoemPage', () => {
  let component: AppendPoemPage;
  let fixture: ComponentFixture<AppendPoemPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AppendPoemPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(AppendPoemPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
