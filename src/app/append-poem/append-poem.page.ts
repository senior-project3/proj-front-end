import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { PoemService } from '../services/poem.service';
import { Subscription } from 'rxjs';
import { FormGroup, FormBuilder, FormControl } from '@angular/forms';
import { AuthService } from '../services/auth.service';


@Component({
  selector: 'app-append-poem',
  templateUrl: './append-poem.page.html',
  styleUrls: ['./append-poem.page.scss'],
})
export class AppendPoemPage implements OnInit {

  poem;
  subscription: Subscription;
  content = [];
  poemForm: FormGroup;
  keySection = [];
  sections: number;
  key = {};
  targetSuggestWordField = "";
  suggestWordList;
  suggestWordEnabled = true;


  constructor(private route: ActivatedRoute, 
              private poemService: PoemService, 
              private formBuilder: FormBuilder, 
              private auth: AuthService,
              private router: Router) { }

  ngOnInit() {
    this.route.paramMap.subscribe(paramMap => {
      const id = paramMap.get('poem_id');
      this.subscription = this.poemService.get(id).subscribe(poem => {
        this.poem = poem;
        this.poem.result.content.map(el => {
          let con = el;
          if(el.length > 0){
            while(con.length < 8) {
              con.push('');
            }  
          }
          this.content.push(con);
        })
        console.log(this.content)
        this.formInit();  
      });
    });
  }

  formReset() {
    this.poemForm = this.formBuilder.group({});
    this.keySection = [];
    this.key = {};
  }

  formInit() {
    this.formReset();
    for(let i=1; i<=this.poem.result.content.length; i++) {
      this.keySection.push(i);
      this.sections = this.poem.result.content.length;
    }
    if(this.poem.poem_type == "กลอนแปดสุภาพ") {
      for(let i = 1; i<=this.sections; i++) {
        this.key['section'+(i)] = [];
        for(let j = 1; j<=8; j++){
          this.key['section'+(i)].push('section'+(i)+'_'+j);
          this.poemForm.addControl('section'+(i)+'_'+j, new FormControl(''));
        }
      } 
    } else {
      for(let i = 1; i<=this.sections; i++) {
        this.key['section'+(i)] = [];
        switch(i%8) {
          case 1:
          case 3:
          case 5:
          case 7: for(let j = 1; j<=5; j++) {
                    this.key['section'+(i)].push('section'+(i)+'_'+j);
                    this.poemForm.addControl('section'+(i)+'_'+j, new FormControl(''));
                  }
                  break;
          case 0:
          case 2:
          case 6: for(let j = 1; j<=4; j++) {
                    this.key['section'+(i)].push('section'+(i)+'_'+j);
                    this.poemForm.addControl('section'+(i)+'_'+j, new FormControl(''));
                  }
                  break;
          case 4: for(let j = 1; j<=2; j++) {
                    this.key['section'+(i)].push('section'+(i)+'_'+j);
                    this.poemForm.addControl('section'+(i)+'_'+j, new FormControl(''));
                  }
                  break;
        }
      }
    }
    console.log(this.keySection)
    console.log(this.key)
  }

  getContent() {
    const content = [];
    if(!this.key) {
      return;
    }
    for(let i=1; i<=this.sections; i++) {
      let sectionString = [];
      if(i > this.content.length){
        continue;
      }
      if(this.content[i-1].length > 0) {
        sectionString = this.content[i-1];
      } else {
        for(let j=1; j<=this.key['section'+i].length; j++){
          if(this.poemForm.controls['section'+i+'_'+j].value != "")
            sectionString.push(this.poemForm.controls['section'+i+'_'+j].value);
        }
      }
      if(sectionString) {
        content.push(sectionString);
      }
    }
    if(this.poem.poem_type == "กลอนแปดสุภาพ"){
      for(let i=0; i<content.length; i+=4) {
        if(content[i].length == 0 && content[i+1].length == 0 && content[i+2].length == 0 && content[i+3].length == 0) {
          for(let j=0; j<4; j++)
            content.pop();
        } 
      } 
    } else {
      for(let i=0; i<content.length; i+=8) {
        if(content[i].length == 0 && content[i+1].length == 0 && content[i+2].length == 0 && content[i+3].length == 0
          && content[i+4].length == 0 && content[i+5].length == 0 && content[i+6].length == 0 && content[i+7].length == 0) {
          for(let j=0; j<8; j++)
            content.pop();
        }  
      } 
    }
    console.log(content);
    return content;
  }

  onSubmit() {
    if(this.poemForm.invalid) {
      return ;
    }

    const data = {
      author: this.auth.getCurrentUser().id,
      content: this.getContent()
    }
    this.subscription = this.poemService.append(data, this.poem._id.$oid)
      .subscribe(
        data => {
          this.poemForm.reset();
          this.formReset();
          this.router.navigateByUrl('tabs/poem/'+data['id']);
      });
  }
  
  addChapter() {
    if(this.poem.poem_type == 'กลอนแปดสุภาพ') {
      for(let i = 1; i<=4; i++) {
        this.keySection.push((i+this.sections));
        this.key['section'+(i+this.sections)] = [];
        for(let j = 1; j<=8; j++){
          this.key['section'+(i+this.sections)].push('section'+(i+this.sections)+'_'+j);
          this.poemForm.addControl('section'+(i+this.sections)+'_'+j, new FormControl(''));
        } 
      }
      this.sections += 4;
    } else {
      for(let i = 1; i<=8; i++) {
        this.keySection.push((i+this.sections));
        this.key['section'+(i+this.sections)] = [];
        // console.log(i+this.sections)
        switch(i%8) {
          case 1:
          case 3:
          case 5:
          case 7: for(let j = 1; j<=5; j++) {
                    this.key['section'+(i+this.sections)].push('section'+(i+this.sections)+'_'+j);
                    this.poemForm.addControl('section'+(i+this.sections)+'_'+j, new FormControl(''));
                  }
                  break;
          case 0:
          case 2:
          case 6: for(let j = 1; j<=4; j++) {
                    this.key['section'+(i+this.sections)].push('section'+(i+this.sections)+'_'+j);
                    this.poemForm.addControl('section'+(i+this.sections)+'_'+j, new FormControl(''));
                  }
                  break;
          case 4: for(let j = 1; j<=2; j++) {
                    this.key['section'+(i+this.sections)].push('section'+(i+this.sections)+'_'+j);
                    this.poemForm.addControl('section'+(i+this.sections)+'_'+j, new FormControl(''));
                  }
                  break;
        }
      }
      this.sections += 8;
    }
    console.log(this.sections)
    console.log(this.keySection)
    console.log(this.key)
  }

  removeChapter() {
    if(this.poem.poem_type == 'กลอนแปดสุภาพ') {
      if(this.sections == 4) {
        return
      }
      for(let i = 0; i<4; i++) {
        this.keySection.pop();
        delete this.key['section'+(this.sections-i)];
        for(let j = 1; j<=8; j++){
          this.poemForm.removeControl('section'+(this.sections-i)+'_'+j);
        } 
      }
      this.sections -= 4;
    } else {
      if(this.sections == 8) {
        return
      }
      for(let i = 0; i<8; i++) {
        this.keySection.pop();
        delete this.key['section'+(this.sections-i)];
        for(let j = 1; j<=8; j++){
          switch((i%8)+1) {
            case 1:
            case 3:
            case 5:
            case 7: for(let j = 1; j<=5; j++) {
                      this.poemForm.removeControl('section'+(this.sections-i)+'_'+j);
                    }
                    break;
            case 0:
            case 2:
            case 6: for(let j = 1; j<=4; j++) {
                      this.poemForm.removeControl('section'+(this.sections-i)+'_'+j);
                    }
                    break;
            case 4: for(let j = 1; j<=2; j++) {
                      this.poemForm.removeControl('section'+(this.sections-i)+'_'+j);
                    }
                    break;
          }
        } 
      }
      this.sections -= 8;
    }
  }

  checkIfSecLessThanContent(sec) {
    // console.log('at',sec)
    return sec < this.content.length? true: false
  }

  suggestWord(sec){
    if(sec.slice(-1) < 3){
      console.log('too few input ')
      return ;
    }
    if(!this.suggestWordEnabled) {
      console.log('wait a bit')
      return ;
    }
    let word1, word2;
    word1 = this.poemForm.controls[sec.substr(0, sec.length - 1) + (parseInt(sec.slice(-1))-2)].value;
    word2 = this.poemForm.controls[sec.substr(0, sec.length - 1) + (parseInt(sec.slice(-1))-1)].value;  
    console.log(word1+word2);
    if(word1.length > 0 && word2.length > 0){
      console.log('should request')
      this.poemService.suggestWord([word1, word2]).subscribe(
        result => {
          this.suggestWordList = result;
          this.targetSuggestWordField = sec;
      }); 
    }
    this.suggestWordEnabled = false;
  }

  completeSuggestWordField(){
    this.targetSuggestWordField = "";
    this.suggestWordList = [];
    this.suggestWordEnabled = true;
  }
}
