import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PoemComponent } from './poem/poem.component';
import { PoemsComponent } from './poems/poems.component';
import { IonicModule } from '@ionic/angular';
import { PoemInfoComponent } from './poem-info/poem-info.component';
import { TimeAgoPipe } from './time-ago.pipe';



@NgModule({
  declarations: [PoemComponent, PoemsComponent, PoemInfoComponent, TimeAgoPipe],
  imports: [
    CommonModule,
    IonicModule,
  ],
  exports: [
    PoemComponent,
    PoemsComponent,
    PoemInfoComponent,
  ]
})
export class ComponentsModule { }
