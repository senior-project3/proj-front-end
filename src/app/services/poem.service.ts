import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { poemQuery } from '../models/poems';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class PoemService {

  constructor(private http: HttpClient) { }

  getAll(page?) {
    let params = new HttpParams()
    if(page) {
      console.log('haspage')
      params = params.set('page', page);
      console.log(params.toString())
    }
    return this.http.get(environment.api_url+'poem/', {params: params});
  } 

  get(poemId) {
    return this.http.get(environment.api_url+`poem/${poemId}`)
  }

  write(poem) {
    return this.http.post(environment.api_url+'poem/write_poem', poem)
  }

  suggestWord(context) {
    console.log(context)
    return this.http.post(environment.api_url+'poem/suggest_word', context)
  }

  getAuthor(id){
    return this.http.get(environment.api_url+`account/user/${id}`)
  }

  append(poem, id) {
    return this.http.post(environment.api_url+`poem/append_poem/${id}`, poem)
  }

  comment(data, id){
    return this.http.post(environment.api_url+`poem/comment/${id}`, data)
  }

  upvote(upvoter, id){
    return this.http.post(environment.api_url+`poem/upvote/${id}`, {'upvoter': upvoter})
  }

  unvote(unvoter, id){
    return this.http.post(environment.api_url+`poem/unvote/${id}`, {'unvoter': unvoter})
  }
  
}
