import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(private http: HttpClient) { }
  private currentUser;

  login(user) {
    // console.log();
    const httpOption = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': 'Basic ' + btoa(user.email+":"+user.password)
      })
    };
    return this.http.get(environment.api_url+`account/get_user/${user.email}`, httpOption);
  }

  isAuthenticated(): boolean {
    return this.currentUser? true : false;
  }

  setCurrentUser(currentUser) {
    this.currentUser = currentUser;
  }

  getCurrentUser() {
    return this.currentUser;
  }

  register(user) {
    return this.http.post(environment.api_url+`account/register`, user);
  }

  logout() {
    this.setCurrentUser(null);
  }

}
