import { Component, OnInit, OnChanges } from '@angular/core';
import { AuthService } from '../services/auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-tabs',
  templateUrl: './tabs.page.html',
  styleUrls: ['./tabs.page.scss'],
})
export class TabsPage implements OnChanges {

  currentUser;

  constructor(private auth: AuthService, private router: Router) { }

  ngOnChanges(){
    console.log(this.currentUser)
  }

  getUserLoggedIn() {
    if(this.currentUser) {
      return true;
    } else {
      return false;
    }
  }

  getCurrentUser() {
    this.currentUser = this.auth.getCurrentUser();
    return this.currentUser? true : false;
  }

  logout() {
    this.auth.logout();
    console.log('logout success')
    this.router.navigateByUrl('/login');
  }
}
