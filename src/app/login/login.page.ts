import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { AuthService } from '../services/auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {

  loginForm: FormGroup;
  loading: boolean = false
  error: boolean = false;

  constructor(private formBuilder: FormBuilder, private auth: AuthService, private router: Router) { }

  ngOnInit() {
    this.loginForm = this.formBuilder.group({
      email: ['', Validators.required],
      password: ['', Validators.required],
    });
  }

  login(){
    if(this.loginForm.invalid){
      this.error = true;
      return
    }
    console.log('login')
    const user = {
      email: this.loginForm.controls.email.value,
      password: this.loginForm.controls.password.value
    };
    this.auth.login(user).subscribe(user => {
      this.auth.setCurrentUser(user);
      this.loginForm.reset();
      this.router.navigateByUrl('/')  
    },
    error => {
      if(error.status == 401) {
        this.error = true;
      }
    });
  }

  changed() {
    this.error = false;
    this.loading = false;
  }
}
